package com.example.hnbexchangewatcher;

import com.vaadin.data.Validator;

public class EmailValidator implements Validator{
	
	/**
	 * Validate Email
	 */
	private static final long serialVersionUID = -8366103802952298649L;
	public boolean isValid(Object value) {
        try {
            validate(value);
        } catch (Validator.InvalidValueException ive) {
            return false;
        }
        return true;
    }
	@Override
	public void validate(Object value) throws Validator.InvalidValueException {
        // Here value is a String containing what *should* be an e-mail address
        if (value instanceof String) {
            String email = (String)value;
            // Match the email string against a (simplistic) regular expression matching e-mail addresses
            if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))
                throw new Validator.InvalidValueException("The e-mail address provided is not valid!");
        }
    }
	
}
