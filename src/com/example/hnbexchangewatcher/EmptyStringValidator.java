package com.example.hnbexchangewatcher;

import com.vaadin.data.Validator;

public class EmptyStringValidator implements Validator{

	/**
	 * Empty String Validator
	 */
	
	private static final long serialVersionUID = -596453333979004900L;
	public boolean isValid(Object value) {
        try {
            validate(value);
        } catch (Validator.InvalidValueException ive) {
            return false;
        }
        return true;
    }
	@Override
	public void validate(Object value) throws InvalidValueException {
		// Here value is a String
        if (value instanceof String) {
            String strValue = (String)value;
            // Check if value is empty
            if (strValue.equals(""))
                throw new Validator.InvalidValueException("Field can't be empty!");
        }
    
	}

}
