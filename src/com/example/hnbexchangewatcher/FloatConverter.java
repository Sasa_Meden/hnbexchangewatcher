package com.example.hnbexchangewatcher;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

public class FloatConverter implements Converter<String, Float>{
	
	/**
	 * Float Converter
	 */
	private static final long serialVersionUID = 4129573228986043470L;
	private Float v;
	private String s;
	String s1 = "0";
	String s2 = "0";
	@Override
	public Float convertToModel(String value,
			Class<? extends Float> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if(value == null || value =="")
			return new Float(0);
		try{		
		value = value.replace(",",".");
		value = value.replace(" ","");
		v = Float.parseFloat(value);
		return v;
		}catch(Exception e){
			throw new ConversionException("Gre�ka prilikom parsiranja koeficijenta", e);
		}
	}

	@Override
	public String convertToPresentation(Float value,
			Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if(value == null)
			return "0.00";
		try{		
			s = value.toString();
			return s;
			}catch(Exception e){
				throw new ConversionException("Gre�ka prilikom parsiranja koeficijenta", e);
			}
		}

	@Override
	public Class<Float> getModelType() {
		return Float.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}

}
