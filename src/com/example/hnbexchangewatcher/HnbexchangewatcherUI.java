package com.example.hnbexchangewatcher;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.annotation.WebServlet;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property.ReadOnlyException;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("hnbexchangewatcher")
public class HnbexchangewatcherUI extends UI {
	private Window LoginWindow;
	private Window SignUpWindow;
	private Window UserSettings;
	private Window YesNoDialog;
	private Window WarningDialog;
	private static Connection conn = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;
	private String QRYUser = "SELECT iduser,password FROM hnbexchangewatcherschema.user WHERE e_mail = @email";
	private String QRYUserData = "SELECT first_name, last_name FROM hnbexchangewatcherschema.user WHERE iduser = @idUser";
	private String QRYAddNewUser = "INSERT INTO `hnbexchangewatcherschema`.`user` (`first_name`, `last_name`, `e_mail`, `password`) VALUES ";//('Brown', 'Wizzard', 'brownwizzard@gmail.com', 'codecons');
	private String QRYGetNewUserID = "SELECT iduser FROM hnbexchangewatcherschema.user ORDER BY iduser DESC LIMIT 1";
	private String QRYSetInitialSettings = "INSERT INTO `hnbexchangewatcherschema`.`user_settings` (`e_mails`, `iduser`) VALUES ";
	private String QRYCheckExisting = "SELECT iduser FROM hnbexchangewatcherschema.user WHERE e_mail = ";
	private String QRYGetParams = "SELECT eur,eur_percentage,usd,usd_percentage,jpy,jpy_percentage,cad,cad_percentage,"
			+ "aud,aud_percentage,gbp,gbp_percentage,czk,czk_percentage,pln,pln_percentage,sek,sek_percentage,"
			+ "chf,chf_percentage,dkk,dkk_percentage,nok,nok_percentage,huf,huf_percentage,e_mails"
			+ " FROM hnbexchangewatcherschema.user_settings WHERE iduser = @idUser";
	private static String QRYGetLastValues = "SELECT eur,usd,jpy,cad,aud,gbp,czk,pln,sek,chf,dkk,nok,huf FROM "
			+ "hnbexchangewatcherschema.curr_values ORDER BY idcurr_values DESC LIMIT 1";

	private static String QRYDeleteUser = "DELETE FROM hnbexchangewatcherschema.user WHERE iduser = @idUser";
	private String QRYUpdateSettings = "";

	private int State = 0;
	private static SimpleDateFormat formatToSend = new SimpleDateFormat("dd.MM.yyyy");

	private Button btnLogin;
	private Button btnSignUp;
	private Button btnSettings;
	private Button btnSignOut;
	private Button btnUnsubscribe;
	private Button btnYes;
	private Button btnNo;
	private Label YesNoDialogMessage;
	private int UserID = -1;
	private String userEmail = "";
	private String userPass ="";
	private String salted="";
	private String userFirstName = "";
	private String userLastName = "";
	private TextField welcomeMsg;

	private VerticalLayout MainWLLayout;
	private HorizontalLayout BtnWLLayout;
	private HorizontalLayout LayoutWelcome;
	private HorizontalLayout TxtLayoutWelcome;
	private HorizontalLayout BtnLayoutWelcome;

	private VerticalLayout MainYesNoDialogLayout;
	private HorizontalLayout YesNoDialogBtnLayout;


	private MyTextField txtWLEmail;
	private PasswordField  txtWLPassword;
	private Button btnWLLogin;
	private Button btnWLCancel ;

	private VerticalLayout MainSULayout;
	private HorizontalLayout BtnSULayout;
	private MyTextField txtSUFirsName;
	private MyTextField txtSULastName;
	private MyTextField txtSUEmail; 
	private PasswordField  txtSUPassword; 
	private PasswordField  txtSUConfirmPassword;
	private Button btnSUSignUp; 
	private Button btnSUCancel; 


	private String signUpFirstName = "";
	private String signUpLastName = "";
	private String signUpEmail = "";
	private String signUpPassword = "";

	private HorizontalLayout BtnLayout;
	private VerticalLayout Mainlayout;


	private VerticalLayout MainSETLayout;
	private HorizontalLayout BtnSETLayout;

	private FormLayout EURSETLayout;
	private FormLayout USDSETLayout;
	private FormLayout JPYSETLayout;
	private FormLayout CADSETLayout;
	private FormLayout AUDSETLayout;
	private FormLayout GBPSETLayout;
	private FormLayout CZKSETLayout;
	private FormLayout PLNSETLayout;
	private FormLayout SEKSETLayout;
	private FormLayout CHFSETLayout;
	private FormLayout DKKSETLayout;
	private FormLayout NOKSETLayout;
	private FormLayout HUFSETLayout;
	private FormLayout EmailSETLayout;

	private HorizontalLayout BtnSETChk1Layout;
	private HorizontalLayout BtnSETChk2Layout;
	private HorizontalLayout BtnSETChk3Layout;

	private static BigDecimal LastEUR;
	private static BigDecimal LastUSD;
	private static BigDecimal LastJPY;
	private static BigDecimal LastCAD;
	private static BigDecimal LastAUD;
	private static BigDecimal LastGBP;
	private static BigDecimal LastCZK;
	private static BigDecimal LastPLN;
	private static BigDecimal LastSEK;
	private static BigDecimal LastCHF;
	private static BigDecimal LastDKK;
	private static BigDecimal LastNOK;
	private static BigDecimal LastHUF;

	private Button btnSETSave;
	private Button btnSETCancel;

	private CheckBox chkEUR;
	private CheckBox chkUSD;
	private CheckBox chkJPY;
	private CheckBox chkCAD;
	private CheckBox chkAUD;
	private CheckBox chkGBP;
	private CheckBox chkCZK;
	private CheckBox chkPLN;
	private CheckBox chkSEK;
	private CheckBox chkCHF;
	private CheckBox chkDKK;
	private CheckBox chkNOK;
	private CheckBox chkHUF;

	private int EUR;
	private int USD;
	private int JPY;
	private int CAD;
	private int AUD;
	private int GBP;
	private int CZK;
	private int PLN;
	private int SEK;
	private int CHF;
	private int DKK;
	private int NOK;
	private int HUF;

	private MyTextField txtPercEUR;
	private MyTextField txtPercUSD;
	private MyTextField txtPercJPY;
	private MyTextField txtPercCAD;
	private MyTextField txtPercAUD;
	private MyTextField txtPercGBP;
	private MyTextField txtPercCZK;
	private MyTextField txtPercPLN;
	private MyTextField txtPercSEK;
	private MyTextField txtPercCHF;
	private MyTextField txtPercDKK;
	private MyTextField txtPercNOK;
	private MyTextField txtPercHUF;
	private MyTextField txtSEEmails;

	private float PercEUR;
	private float PercUSD;
	private float PercJPY;
	private float PercCAD;
	private float PercAUD;
	private float PercGBP;
	private float PercCZK;
	private float PercPLN;
	private float PercSEK;
	private float PercCHF;
	private float PercDKK;
	private float PercNOK;
	private float PercHUF;

	private String ParamEmails;



	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = HnbexchangewatcherUI.class)
	public static class Servlet extends VaadinServlet {
	}
	@Override
	protected void init(VaadinRequest request) {
		State = 0;
		BtnLayout = new HorizontalLayout();
		Mainlayout = new VerticalLayout();
		Mainlayout.setMargin(true);
		btnLogin = new Button("Login");
		btnLogin.addStyleName("login");


		btnLogin.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				CreateLoginWindow();
			}
		});
		btnSignUp = new Button("Sign Up");
		btnSignUp.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				CreateSignUpWindow();
			}
		});
		//BtnLayout.setSizeFull();
		BtnLayout.setSpacing(true);
		BtnLayout.addComponent(btnLogin);
		BtnLayout.addComponent(btnSignUp);
		BtnLayout.setComponentAlignment(btnLogin, Alignment.TOP_RIGHT);
		BtnLayout.setComponentAlignment(btnSignUp, Alignment.TOP_RIGHT);
		Mainlayout.addComponent(BtnLayout);
		Mainlayout.setComponentAlignment(BtnLayout,Alignment.TOP_RIGHT);
		//layout.addComponent(button);
		setContent(Mainlayout);
	}
	private void UpdateSettings(int state){
		int i = 0;
		boolean enabled = false;
		boolean checked = false;

		if (txtPercEUR.isValid()&&txtPercUSD.isValid()&&txtPercJPY.isValid()&&txtPercCAD.isValid()
				&&txtPercAUD.isValid()&&txtPercGBP.isValid()&&txtPercCZK.isValid()&&txtPercPLN.isValid()&&txtPercSEK.isValid()
				&&txtPercCHF.isValid()&&txtPercDKK.isValid()&&txtPercNOK.isValid()&&txtPercHUF.isValid()&&txtSEEmails.isValid()&&UserID >-1){
			checked = true;
		}else{
			checked = false;
		}
			if(chkEUR.getValue()){
				EUR = 1;
				PercEUR = (float) txtPercEUR.getConvertedValue();
			}else{
				EUR = 0;
				PercEUR = (float) txtPercEUR.getConvertedValue();
			}
			if(chkUSD.getValue()){
				USD = 1;
				PercUSD = (float) txtPercUSD.getConvertedValue();
			}else{
				USD = 0;
				PercUSD = (float) txtPercUSD.getConvertedValue();
			}
			if(chkJPY.getValue()){
				JPY = 1;
				PercJPY = (float) txtPercJPY.getConvertedValue();
			}else{
				JPY = 0;
				PercJPY = (float) txtPercJPY.getConvertedValue();
			}
			if(chkCAD.getValue()){
				CAD = 1;
				PercCAD = (float) txtPercCAD.getConvertedValue();
			}else{
				CAD = 0;
				PercCAD = (float) txtPercCAD.getConvertedValue();
			}
			if(chkAUD.getValue()){
				AUD = 1;
				PercAUD = (float) txtPercAUD.getConvertedValue();
			}else{
				AUD = 0;
				PercAUD = (float) txtPercAUD.getConvertedValue();
			}
			if(chkGBP.getValue()){
				GBP = 1;
				PercGBP = (float) txtPercGBP.getConvertedValue();
			}else{
				GBP = 0;
				PercGBP = (float) txtPercGBP.getConvertedValue();
			}
			if(chkCZK.getValue()){
				CZK = 1;
				PercCZK = (float) txtPercCZK.getConvertedValue();
			}else{
				CZK = 0;
				PercCZK = (float) txtPercCZK.getConvertedValue();
			}
			if(chkPLN.getValue()){
				PLN = 1;
				PercPLN = (float) txtPercPLN.getConvertedValue();
			}else{
				PLN = 0;
				PercPLN = (float) txtPercPLN.getConvertedValue();
			}
			if(chkSEK.getValue()){
				SEK = 1;
				PercSEK = (float) txtPercSEK.getConvertedValue();
			}else{
				SEK = 0;
				PercSEK = (float) txtPercSEK.getConvertedValue();
			}
			if(chkCHF.getValue()){
				CHF = 1;
				PercCHF = (float) txtPercCHF.getConvertedValue();
			}else{
				CHF = 0;
				PercCHF = (float) txtPercCHF.getConvertedValue();
			}
			if(chkDKK.getValue()){
				DKK = 1;
				PercDKK = (float) txtPercDKK.getConvertedValue();
			}else{
				DKK = 0;
				PercDKK = (float) txtPercDKK.getConvertedValue();
			}
			if(chkNOK.getValue()){
				NOK = 1;
				PercNOK = (float) txtPercNOK.getConvertedValue();
			}else{
				NOK = 0;
				PercNOK = (float) txtPercNOK.getConvertedValue();
			}
			if(chkHUF.getValue()){
				HUF = 1;
				PercHUF = (float) txtPercHUF.getConvertedValue();
			}else{
				HUF = 0;
				PercHUF = (float) txtPercHUF.getConvertedValue();
			}
			ParamEmails = txtSEEmails.getValue();
			enabled = CheckIfCurEnabled();
			if(!enabled){
				OpenWarninigDialog("You have to enable at least one currency\nto recieve e-mail");
			}else if(checked){
			try{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
				String connectionUser = "root";
				String connectionPassword = "Code_Cons_Rulez1";
				conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
				stmt = (Statement) conn.createStatement();
				QRYUpdateSettings = "UPDATE `hnbexchangewatcherschema`.`user_settings` SET"
						+ " `eur`='"+EUR+"', `eur_percentage`='"+PercEUR+"', `usd`='"+USD+"', `usd_percentage`='"+PercUSD+"', `jpy`='"+JPY+"',"
						+ " `jpy_percentage`='"+PercJPY+"', `cad`='"+CAD+"', `cad_percentage`='"+PercCAD+"', `aud`='"+AUD+"', `aud_percentage`='"+PercAUD+"',"
						+ " `gbp`='"+GBP+"', `gbp_percentage`='"+PercGBP+"', `czk`='"+CZK+"', `czk_percentage`='"+PercCZK+"', `pln`='"+PLN+"', `pln_percentage`='"+PercPLN+"',"
						+ " `sek`='"+SEK+"', `sek_percentage`='"+PercSEK+"', `chf`='"+CHF+"', `chf_percentage`='"+PercCHF+"', `dkk`='"+DKK+"', `dkk_percentage`='"+PercDKK+"',"
						+ " `nok`='"+NOK+"', `nok_percentage`='"+PercNOK+"', `huf`='"+HUF+"', `huf_percentage`='"+PercHUF+"', `e_mails`='"+ParamEmails+"' WHERE `iduser`= "+UserID;

				i = stmt.executeUpdate(QRYUpdateSettings);


			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
				try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
				try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
				UserSettings.close();
				if(state == 0){
					SendWelcomeMail();
				}
			}
		}else if(!checked){
			Notification.show("Error","Not all fields are valid!",Notification.Type.WARNING_MESSAGE);
		}
		
	}

	private void SendWelcomeMail(){
		String msgDate = formatToSend.format(new Date());
		String tosend="";
		String subject;
		String message;
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		// Sender's email ID needs to be mentioned
		String from = "hnbexchange.watcher@gmail.com";
		// Assuming you are sending email from localhost
		String host = "localhost";
		// Get system properties
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", "smtp.gmail.com");
		properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		properties.setProperty("mail.smtp.socketFactory.fallback", "false");
		properties.setProperty("mail.smtp.port", "465");
		properties.setProperty("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.debug", "true");
		properties.put("mail.store.protocol", "pop3");
		properties.put("mail.transport.protocol", "smtp");
		final String username = from;
		final String password = "hnb_Exchange1020304050";
		subject = "Welcome to Exchange Watcher "+msgDate;
		message = "Currencie you selected to follow:\n";
		getLast();
		if(EUR>0){
			tosend += "EUR: "+LastEUR;
		}
		if(USD>0){
			tosend +="\nUSD: "+LastUSD;
		}
		if(JPY>0){
			tosend +="\nJPY: "+LastJPY;
		}
		if(CAD>0){
			tosend +="\nCAD: "+LastCAD;
		}
		if(AUD>0){
			tosend +="\nAUD: "+LastAUD;
		}
		if(GBP>0){
			tosend +="\nGBP: "+LastGBP;
		}
		if(CZK>0){
			tosend +="\nCZK: "+LastCZK;
		}
		if(PLN>0){
			tosend +="\nPLN: "+LastPLN;
		}
		if(SEK>0){
			tosend +="\nSEK: "+LastSEK;
		}
		if(CHF>0){
			tosend +="\nCHF: "+LastCHF;
		}
		if(DKK>0){
			tosend +="\nDKK: "+LastDKK;
		}
		if(NOK>0){
			tosend +="\nNOK: "+LastNOK;
		}
		if(HUF>0){
			tosend +="\nHUF: "+LastHUF;
		}

		message += tosend;
		try{

			Session session = Session.getDefaultInstance(properties,new Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}});

			// -- Create a new message --
			Message msg = new MimeMessage(session);

			// -- Set the FROM and TO fields --
			msg.setFrom(new InternetAddress(from));
			msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse(ParamEmails,false));
			msg.setSubject(subject);
			msg.setText(message);
			msg.setSentDate(new Date());
			Transport.send(msg);
			//System.out.println("Message sent.");


		}catch (MessagingException e){ 
			SaveErrorLog("SendEmail()\nMessagingException", e.toString());
			//System.out.println("Error cause: " + e);
		}
	}
	private static void SaveErrorLog(String function,String error){

	}

	public static void getLast(){
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
			String connectionUser = "root";
			String connectionPassword = "Code_Cons_Rulez1";
			conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = (Statement) conn.createStatement();
			rs = stmt.executeQuery(QRYGetLastValues);
			while (rs.next()) {
				LastEUR = rs.getBigDecimal(1);
				LastUSD = rs.getBigDecimal(2);
				LastJPY = rs.getBigDecimal(3);
				LastCAD = rs.getBigDecimal(4);
				LastAUD = rs.getBigDecimal(5);
				LastGBP = rs.getBigDecimal(6);
				LastCZK = rs.getBigDecimal(7);
				LastPLN = rs.getBigDecimal(8);
				LastSEK = rs.getBigDecimal(9);
				LastCHF = rs.getBigDecimal(10);
				LastDKK = rs.getBigDecimal(11);
				LastNOK = rs.getBigDecimal(12);
				LastHUF = rs.getBigDecimal(13);

			}

		} catch (Exception e) {
			SendErrorLog("getLast()",e.toString());
			//e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { 
				SendErrorLog("getLast()\nSQLException",e.toString());
				//e.printStackTrace();
			}
			try { if (stmt != null) stmt.close(); } catch (SQLException e) {
				SendErrorLog("getLast()\nSQLException",e.toString());
				//e.printStackTrace(); 
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				SendErrorLog("getLast()\nSQLException",e.toString());
				//e.printStackTrace();
			}
		}	

	}

	public static void SendErrorLog(String function,String error){
		Date msgDate = new Date();
		String tosend="";
		String subject;
		String message;
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		// Sender's email ID needs to be mentioned
		String from = "hnbexchange.watcher@gmail.com";
		// Assuming you are sending email from localhost
		String host = "localhost";
		// Get system properties
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", "smtp.gmail.com");
		properties.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		properties.setProperty("mail.smtp.socketFactory.fallback", "false");
		properties.setProperty("mail.smtp.port", "465");
		properties.setProperty("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.debug", "true");
		properties.put("mail.store.protocol", "pop3");
		properties.put("mail.transport.protocol", "smtp");
		final String username = from;
		final String password = "hnb_Exchange1020304050";
		subject = "Exchange Watcher - ERROR " +msgDate;
		message = msgDate +"\n"+function+"\n"+error;
		try{
			Session session = Session.getDefaultInstance(properties,new Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}});

			// -- Create a new message --
			Message msg = new MimeMessage(session);

			// -- Set the FROM and TO fields --
			msg.setFrom(new InternetAddress(from));
			msg.setRecipients(Message.RecipientType.TO,InternetAddress.parse("sasa.meden@gmail.com",false));
			msg.setSubject(subject);
			msg.setText(message);
			msg.setSentDate(new Date());
			Transport.send(msg);
			System.out.println("Message sent.");


		}catch (MessagingException e){ 
			//System.out.println("Error cause: " + e);
			SaveErrorLog("SendErrorLog()\nMessagingException",e.toString());
		}

	}
	private void CreateSettingsWindow(int state){

		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
			String connectionUser = "root";
			String connectionPassword = "Code_Cons_Rulez1";
			conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = (Statement) conn.createStatement();
			rs = stmt.executeQuery(param(QRYGetParams,UserID));
			while (rs.next()) {
				EUR = rs.getInt(1);
				PercEUR = rs.getFloat(2);
				USD = rs.getInt(3);
				PercUSD =rs.getFloat(4);
				JPY = rs.getInt(5);
				PercJPY = rs.getFloat(6);
				CAD = rs.getInt(7);
				PercCAD = rs.getFloat(8);
				AUD = rs.getInt(9);
				PercAUD = rs.getFloat(10);
				GBP = rs.getInt(11);
				PercGBP = rs.getFloat(12);
				CZK = rs.getInt(13);
				PercCZK = rs.getFloat(14);
				PLN = rs.getInt(15);
				PercPLN = rs.getFloat(16);
				SEK = rs.getInt(17);
				PercSEK = rs.getFloat(18);
				CHF = rs.getInt(19);
				PercCHF = rs.getFloat(20);
				DKK = rs.getInt(21);
				PercDKK = rs.getFloat(22);
				NOK = rs.getInt(23);
				PercNOK = rs.getFloat(24);
				HUF = rs.getInt(25);
				PercHUF = rs.getFloat(26);
				ParamEmails = rs.getString(27);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) {
				e.printStackTrace(); 
			}
			try { if (stmt != null) stmt.close(); } catch (SQLException e) {
				e.printStackTrace();
			}
			try { if (conn != null) conn.close(); } catch (SQLException e) {
				e.printStackTrace();
			}

			MainSETLayout = new VerticalLayout();
			BtnSETLayout = new HorizontalLayout();

			EURSETLayout = new FormLayout();
			USDSETLayout = new FormLayout();
			JPYSETLayout = new FormLayout();
			CADSETLayout = new FormLayout();
			AUDSETLayout = new FormLayout();
			GBPSETLayout = new FormLayout();
			CZKSETLayout = new FormLayout();
			PLNSETLayout = new FormLayout();
			SEKSETLayout = new FormLayout();
			CHFSETLayout = new FormLayout();
			DKKSETLayout = new FormLayout();
			NOKSETLayout = new FormLayout();
			HUFSETLayout = new FormLayout();
			EmailSETLayout = new FormLayout();

			BtnSETChk1Layout = new HorizontalLayout();
			BtnSETChk2Layout = new HorizontalLayout();
			BtnSETChk3Layout = new HorizontalLayout();


			btnSETSave = new Button("Save");
			btnSETCancel = new Button("Cancel");
			btnSETSave.setStyleName("login");

			chkEUR = new CheckBox("EUR");
			chkUSD = new CheckBox("USD");
			chkJPY = new CheckBox("JPY");
			chkCAD = new CheckBox("CAD");
			chkAUD = new CheckBox("AUD");
			chkGBP = new CheckBox("GBP");
			chkCZK = new CheckBox("CZK");
			chkPLN = new CheckBox("PLN");
			chkSEK = new CheckBox("SEK");
			chkCHF = new CheckBox("CHF");
			chkDKK = new CheckBox("DKK");
			chkNOK = new CheckBox("NOK");
			chkHUF = new CheckBox("HUF");

			txtPercEUR = new MyTextField("");
			txtPercUSD = new MyTextField("");
			txtPercJPY = new MyTextField("");
			txtPercCAD = new MyTextField("");
			txtPercAUD = new MyTextField("");
			txtPercGBP = new MyTextField("");
			txtPercCZK = new MyTextField("");
			txtPercPLN = new MyTextField("");
			txtPercSEK = new MyTextField("");
			txtPercCHF = new MyTextField("");
			txtPercDKK = new MyTextField("");
			txtPercNOK = new MyTextField("");
			txtPercHUF = new MyTextField("");
			txtSEEmails = new MyTextField("E-mail:");
			txtSEEmails.setInputPrompt("Enter E-mail...");

			txtPercEUR.setConverter(new FloatConverter());
			txtPercUSD.setConverter(new FloatConverter());
			txtPercJPY.setConverter(new FloatConverter());
			txtPercCAD.setConverter(new FloatConverter());
			txtPercAUD.setConverter(new FloatConverter());
			txtPercGBP.setConverter(new FloatConverter());
			txtPercCZK.setConverter(new FloatConverter());
			txtPercPLN.setConverter(new FloatConverter());
			txtPercSEK.setConverter(new FloatConverter());
			txtPercCHF.setConverter(new FloatConverter());
			txtPercDKK.setConverter(new FloatConverter());
			txtPercNOK.setConverter(new FloatConverter());
			txtPercHUF.setConverter(new FloatConverter());

			txtSEEmails.addValidator(new EmailValidator());
			txtSEEmails.setImmediate(true);

			txtPercEUR.setImmediate(true);
			txtPercUSD.setImmediate(true);
			txtPercJPY.setImmediate(true);
			txtPercCAD.setImmediate(true);
			txtPercAUD.setImmediate(true);
			txtPercGBP.setImmediate(true);
			txtPercCZK.setImmediate(true);
			txtPercPLN.setImmediate(true);
			txtPercSEK.setImmediate(true);
			txtPercCHF.setImmediate(true);
			txtPercDKK.setImmediate(true);
			txtPercNOK.setImmediate(true);
			txtPercHUF.setImmediate(true);

			if(EUR > 0){
				chkEUR.setValue(true);
				txtPercEUR.setEnabled(true);
			}else{
				chkEUR.setValue(false);
				txtPercEUR.setEnabled(false);
			}
			if(USD > 0){
				chkUSD.setValue(true);
				txtPercUSD.setEnabled(true);
			}else{
				chkUSD.setValue(false);
				txtPercUSD.setEnabled(false);
			}
			if(JPY > 0){
				chkJPY.setValue(true);
				txtPercJPY.setEnabled(true);
			}else{
				chkJPY.setValue(false);
				txtPercJPY.setEnabled(false);
			}
			if(CAD > 0){
				chkCAD.setValue(true);
				txtPercCAD.setEnabled(true);
			}else{
				chkCAD.setValue(false);
				txtPercCAD.setEnabled(false);
			}
			if(AUD > 0){
				chkAUD.setValue(true);
				txtPercAUD.setEnabled(true);
			}else{
				chkAUD.setValue(false);
				txtPercAUD.setEnabled(false);
			}
			if(GBP > 0){
				chkGBP.setValue(true);
				txtPercGBP.setEnabled(true);
			}else{
				chkGBP.setValue(false);
				txtPercGBP.setEnabled(false);
			}
			if(CZK > 0){
				chkCZK.setValue(true);
				txtPercCZK.setEnabled(true);
			}else{
				chkCZK.setValue(false);
				txtPercCZK.setEnabled(false);
			}
			if(PLN > 0){
				chkPLN.setValue(true);
				txtPercPLN.setEnabled(true);
			}else{
				chkPLN.setValue(false);
				txtPercPLN.setEnabled(false);
			}
			if(SEK > 0){
				chkSEK.setValue(true);
				txtPercSEK.setEnabled(true);
			}else{
				chkSEK.setValue(false);
				txtPercSEK.setEnabled(false);
			}
			if(CHF > 0){
				chkCHF.setValue(true);
				txtPercCHF.setEnabled(true);
			}else{
				chkCHF.setValue(false);
				txtPercCHF.setEnabled(false);
			}
			if(DKK > 0){
				chkDKK.setValue(true);
				txtPercDKK.setEnabled(true);
			}else{
				chkDKK.setValue(false);
				txtPercDKK.setEnabled(false);
			}
			if(NOK > 0){
				chkNOK.setValue(true);
				txtPercNOK.setEnabled(true);
			}else{
				chkNOK.setValue(false);
				txtPercNOK.setEnabled(false);
			}
			if(HUF > 0){
				chkHUF.setValue(true);
				txtPercHUF.setEnabled(true);
			}else{
				chkHUF.setValue(false);
				txtPercHUF.setEnabled(false);
			}
			txtPercEUR.setValue(PercEUR);
			txtPercUSD.setValue(PercUSD);
			txtPercJPY.setValue(PercJPY);
			txtPercCAD.setValue(PercCAD);
			txtPercAUD.setValue(PercAUD);
			txtPercGBP.setValue(PercGBP);
			txtPercCZK.setValue(PercCZK);
			txtPercPLN.setValue(PercPLN);
			txtPercSEK.setValue(PercSEK);
			txtPercCHF.setValue(PercCHF);
			txtPercDKK.setValue(PercDKK);
			txtPercNOK.setValue(PercNOK);
			txtPercHUF.setValue(PercHUF);
			txtSEEmails.setValue(ParamEmails);

			txtPercEUR.setWidth("60px");
			txtPercUSD.setWidth("60px");
			txtPercJPY.setWidth("60px");
			txtPercCAD.setWidth("60px");
			txtPercAUD.setWidth("60px");
			txtPercGBP.setWidth("60px");
			txtPercCZK.setWidth("60px");
			txtPercPLN.setWidth("60px");
			txtPercSEK.setWidth("60px");
			txtPercCHF.setWidth("60px");
			txtPercDKK.setWidth("60px");
			txtPercNOK.setWidth("60px");
			txtPercHUF.setWidth("60px");
			txtSEEmails.setWidth("335px");


			chkEUR.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {


					if(chkEUR.getValue()){
						EUR = 0;
						txtPercEUR.setEnabled(true);
					}else{
						EUR = 1;
						txtPercEUR.setEnabled(false);
						txtPercEUR.setValue(PercEUR);

					}
				}
			});
			chkUSD.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkUSD.getValue()){
						USD = 0;
						txtPercUSD.setEnabled(true);
					}else{
						USD = 1;
						txtPercUSD.setEnabled(false);
						txtPercUSD.setValue(PercUSD);

					}
				}
			});
			chkJPY.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkJPY.getValue()){
						JPY = 0;
						txtPercJPY.setEnabled(true);
					}else{
						JPY = 1;
						txtPercJPY.setEnabled(false);
						txtPercJPY.setValue(PercJPY);

					}
				}
			});
			chkCAD.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkCAD.getValue()){
						CAD = 0;
						txtPercCAD.setEnabled(true);
					}else{
						CAD = 1;
						txtPercCAD.setEnabled(false);
						txtPercCAD.setValue(PercCAD);

					}
				}
			});
			chkAUD.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkAUD.getValue()){
						AUD = 0;
						txtPercAUD.setEnabled(true);
					}else{
						AUD = 1;
						txtPercAUD.setEnabled(false);
						txtPercAUD.setValue(PercAUD);

					}
				}
			});
			chkGBP.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkGBP.getValue()){
						GBP = 0;
						txtPercGBP.setEnabled(true);
					}else{
						GBP = 1;
						txtPercGBP.setEnabled(false);
						txtPercGBP.setValue(PercGBP);

					}
				}
			});
			chkCZK.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkCZK.getValue()){
						CZK = 0;
						txtPercCZK.setEnabled(true);
					}else{
						CZK = 1;
						txtPercCZK.setEnabled(false);
						txtPercCZK.setValue(PercCZK);

					}
				}
			});
			chkPLN.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkPLN.getValue()){
						PLN = 0;
						txtPercPLN.setEnabled(true);
					}else{
						PLN = 1;
						txtPercPLN.setEnabled(false);
						txtPercPLN.setValue(PercPLN);

					}
				}
			});
			chkSEK.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkSEK.getValue()){
						SEK = 0;
						txtPercSEK.setEnabled(true);
					}else{
						SEK = 1;
						txtPercSEK.setEnabled(false);
						txtPercSEK.setValue(PercSEK);

					}
				}
			});
			chkCHF.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkCHF.getValue()){
						CHF = 0;
						txtPercCHF.setEnabled(true);
					}else{
						CHF = 1;
						txtPercCHF.setEnabled(false);
						txtPercCHF.setValue(PercCHF);

					}
				}
			});
			chkDKK.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkDKK.getValue()){
						DKK = 0;
						txtPercDKK.setEnabled(true);
					}else{
						DKK = 1;
						txtPercDKK.setEnabled(false);
						txtPercDKK.setValue(PercDKK);

					}
				}
			});
			chkNOK.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkNOK.getValue()){
						NOK = 0;
						txtPercNOK.setEnabled(true);
					}else{
						NOK = 1;
						txtPercNOK.setEnabled(false);
						txtPercNOK.setValue(PercNOK);

					}
				}
			});
			chkHUF.addValueChangeListener(new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					if(chkHUF.getValue()){
						HUF = 0;
						txtPercHUF.setEnabled(true);
					}else{
						HUF = 1;
						txtPercHUF.setEnabled(false);
						txtPercHUF.setValue(PercHUF);

					}
				}
			});

			btnSETCancel.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					
					if(!CheckIfCurEnabled()){
						OpenWarninigDialog("You have to enable at least one currency\nto recieve e-mail");
					}else{
					UserSettings.close();
					}
				}
			});
			btnSETSave.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					UpdateSettings(state);
				}
			});

			EURSETLayout.addComponent(chkEUR);
			EURSETLayout.addComponent(txtPercEUR);
			USDSETLayout.addComponent(chkUSD);
			USDSETLayout.addComponent(txtPercUSD);
			JPYSETLayout.addComponent(chkJPY);
			JPYSETLayout.addComponent(txtPercJPY);
			CADSETLayout.addComponent(chkCAD);
			CADSETLayout.addComponent(txtPercCAD);
			AUDSETLayout.addComponent(chkAUD);
			AUDSETLayout.addComponent(txtPercAUD);
			GBPSETLayout.addComponent(chkGBP);
			GBPSETLayout.addComponent(txtPercGBP);
			CZKSETLayout.addComponent(chkCZK);
			CZKSETLayout.addComponent(txtPercCZK);
			PLNSETLayout.addComponent(chkPLN);
			PLNSETLayout.addComponent(txtPercPLN);
			SEKSETLayout.addComponent(chkSEK);
			SEKSETLayout.addComponent(txtPercSEK);
			CHFSETLayout.addComponent(chkCHF);
			CHFSETLayout.addComponent(txtPercCHF);
			DKKSETLayout.addComponent(chkDKK);
			DKKSETLayout.addComponent(txtPercDKK);
			NOKSETLayout.addComponent(chkNOK);
			NOKSETLayout.addComponent(txtPercNOK);
			HUFSETLayout.addComponent(chkHUF);
			HUFSETLayout.addComponent(txtPercHUF);
			EmailSETLayout.addComponent(txtSEEmails);
			EmailSETLayout.setMargin(true);

			BtnSETChk1Layout.setSizeFull();
			BtnSETChk1Layout.addComponent(EURSETLayout);
			BtnSETChk1Layout.addComponent(USDSETLayout);
			BtnSETChk1Layout.addComponent(JPYSETLayout);
			BtnSETChk1Layout.addComponent(CADSETLayout);
			BtnSETChk1Layout.addComponent(AUDSETLayout);
			BtnSETChk1Layout.addComponent(GBPSETLayout);

			BtnSETChk2Layout.setSizeFull();
			BtnSETChk2Layout.addComponent(CZKSETLayout);
			BtnSETChk2Layout.addComponent(PLNSETLayout);
			BtnSETChk2Layout.addComponent(SEKSETLayout);
			BtnSETChk2Layout.addComponent(CHFSETLayout);
			BtnSETChk2Layout.addComponent(DKKSETLayout);
			BtnSETChk2Layout.addComponent(NOKSETLayout);


			BtnSETChk3Layout.setSpacing(true);
			BtnSETChk3Layout.addComponent(HUFSETLayout);
			BtnSETChk3Layout.addComponent(EmailSETLayout);
			BtnSETChk3Layout.setComponentAlignment(HUFSETLayout, Alignment.BOTTOM_LEFT);
			BtnSETChk3Layout.setComponentAlignment(EmailSETLayout, Alignment.BOTTOM_RIGHT);

			BtnSETLayout.setSizeFull();
			BtnSETLayout.addComponent(btnSETSave);
			BtnSETLayout.addComponent(btnSETCancel);
			BtnSETLayout.setComponentAlignment(btnSETSave, Alignment.BOTTOM_LEFT);
			BtnSETLayout.setComponentAlignment(btnSETCancel, Alignment.BOTTOM_RIGHT);
			BtnSETLayout.setMargin(true);

			MainSETLayout.addComponent(BtnSETChk1Layout);
			MainSETLayout.addComponent(BtnSETChk2Layout);
			MainSETLayout.addComponent(BtnSETChk3Layout);
			MainSETLayout.addComponent(BtnSETLayout);

			UserSettings = new Window();
			UserSettings.setStyleName("windialog");
			UserSettings.setModal(true);
			addWindow(UserSettings);
			UserSettings.setWidth("540px");
			UserSettings.setCaption("SETTINGS");
			UserSettings.setHeightUndefined();
			UserSettings.setResizable(false);
			UserSettings.center();
			UserSettings.setContent(MainSETLayout);

		}	
	}


	public boolean Exists(){
		int emailExists = -1;
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
			String connectionUser = "root";
			String connectionPassword = "Code_Cons_Rulez1";
			conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
			stmt = (Statement) conn.createStatement();
			QRYCheckExisting = "SELECT iduser FROM hnbexchangewatcherschema.user WHERE e_mail = ";
			QRYCheckExisting = QRYCheckExisting+ "'"+signUpEmail+"'";
			rs = stmt.executeQuery(QRYCheckExisting);
			while (rs.next()) {
				emailExists = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		if(emailExists > -1 ){
			return true;
		}else{
			return false;
		}

	}

	private void CreateSignUpWindow(){
		MainSULayout = new VerticalLayout();
		BtnSULayout = new HorizontalLayout();
		txtSUFirsName = new MyTextField("First name:");
		txtSUFirsName.setSizeFull();
		txtSUFirsName.setInputPrompt("Enter First Name...");
		txtSULastName = new MyTextField("Last name");
		txtSULastName.setSizeFull();
		txtSULastName.setInputPrompt("Enter Last Name...");
		txtSUEmail = new MyTextField("E-mail:");
		txtSUEmail.setSizeFull();
		txtSUEmail.setInputPrompt("Enter E-mail...");
		txtSUPassword = new PasswordField ("Password: (6 characters minimum)");
		txtSUPassword.setSizeFull();
		txtSUPassword.setInputPrompt("Enter Password...");
		txtSUConfirmPassword = new PasswordField ("Confirm password:");
		txtSUConfirmPassword.setSizeFull();
		txtSUConfirmPassword.setInputPrompt("Confirm Password...");
		txtSUConfirmPassword.setImmediate(true);
		txtSUFirsName.setRequired(true);
		txtSULastName.setRequired(true);
		txtSUEmail.setRequired(true);
		txtSUPassword.setRequired(true);
		txtSUConfirmPassword.setRequired(true);
		txtSUEmail.setImmediate(true);
		btnSUSignUp = new Button("Sign up");
		btnSUCancel = new Button("Cancel");
		txtSUFirsName.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				txtSUFirsName.setComponentError(null);
				txtSUFirsName.setValidationVisible(false);
				txtSUFirsName.addValidator(new EmptyStringValidator());
				txtSUFirsName.validate();
				signUpFirstName = txtSUFirsName.getValue();

			}
		});
		txtSULastName.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				txtSULastName.setComponentError(null);
				txtSULastName.setValidationVisible(false);
				txtSULastName.addValidator(new EmptyStringValidator());
				txtSULastName.validate();
				signUpLastName = txtSULastName.getValue();

			}
		});
		txtSUEmail.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				txtSUEmail.setComponentError(null);
				txtSUEmail.setValidationVisible(false);
				txtSUEmail.addValidator(new EmailValidator());
				txtSUEmail.validate();
				signUpEmail = txtSUEmail.getValue();
			}
		});
		txtSUPassword.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				txtSUPassword.setComponentError(null);
				txtSUPassword.setValidationVisible(false);
				txtSUPassword.addValidator(new PassLenValidator());
				txtSUPassword.validate();
				signUpPassword = txtSUPassword.getValue();
			}
		});
		txtSUConfirmPassword.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				txtSUConfirmPassword.setComponentError(null);
				txtSUConfirmPassword.setValidationVisible(false);
				txtSUConfirmPassword.addValidator(new PasswordValidator(txtSUPassword.getValue()));
				txtSUConfirmPassword.validate();
				signUpPassword = txtSUPassword.getValue();
			}
		});
		
		
		OnEnterKeyHandler onEnterHandler2 = new OnEnterKeyHandler(){
            @Override
            public void onEnterKeyPressed() {
            	btnSUSignUp.click();
            }
        };
        onEnterHandler2.installOn(txtSUConfirmPassword);
		
        btnSUSignUp.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				//boolean valid = true;
				if(txtSUEmail.isValid()){

					if(Exists()==true){
						Notification.show("Error","User with this E-mail already exists!",Notification.Type.WARNING_MESSAGE);
						txtSUEmail.focus();
					}else{
						if(txtSUFirsName.isValid()&& txtSULastName.isValid()&&txtSUEmail.isValid()&&txtSUPassword.isValid()&&txtSUConfirmPassword.isValid()){
							@SuppressWarnings("unused")
							int i = 0;
							int NewUserID = -1;
							try{
								Class.forName("com.mysql.jdbc.Driver").newInstance();
								String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
								String connectionUser = "root";
								String connectionPassword = "Code_Cons_Rulez1";
								conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
								stmt = (Statement) conn.createStatement();
								QRYAddNewUser = "INSERT INTO `hnbexchangewatcherschema`.`user` (`first_name`, `last_name`, `e_mail`, `password`) VALUES ";
								QRYAddNewUser = QRYAddNewUser + "('"+signUpFirstName+"', '"+signUpLastName+"', '"+signUpEmail+"', '"+generateStrongPasswordHash(signUpPassword)+"')";
								i = stmt.executeUpdate(QRYAddNewUser);
								rs = stmt.executeQuery(QRYGetNewUserID);
								while (rs.next()) {
									NewUserID = rs.getInt(1);
								}
								if(NewUserID > -1){
									QRYSetInitialSettings = "INSERT INTO `hnbexchangewatcherschema`.`user_settings` (`e_mails`, `iduser`) VALUES ";
									QRYSetInitialSettings = QRYSetInitialSettings + "('"+signUpEmail+"', '"+NewUserID+"')";
									i = stmt.executeUpdate(QRYSetInitialSettings);

								}

							} catch (Exception e) {
								e.printStackTrace();
							} finally {
								try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
								try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
								try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
							}
							UserID = NewUserID;
							SignUpWindow.close();
							LayoutWelcome = new HorizontalLayout();
							TxtLayoutWelcome = new HorizontalLayout();
							BtnLayoutWelcome = new HorizontalLayout();
							welcomeMsg = new TextField();
							welcomeMsg.setWidth("100%");
							welcomeMsg.setStyleName("lblwelcome");
							welcomeMsg.setValue("Welcome, "+signUpFirstName+" "+signUpLastName);
							welcomeMsg.setEnabled(false);
							btnSettings = new Button("Settings");
							btnUnsubscribe = new Button("Unsubscribe");
							btnUnsubscribe.setStyleName("unsubscribe");
							btnSettings.setStyleName("settings");
							btnSignOut = new Button("Sign Out");
							btnSettings.addClickListener(new Button.ClickListener() {
								public void buttonClick(ClickEvent event) {
									CreateSettingsWindow(1);
								}
							});
							btnSignOut.addClickListener(new Button.ClickListener() {
								public void buttonClick(ClickEvent event) {
									Mainlayout.removeAllComponents();
									Mainlayout.addComponent(BtnLayout);
									Mainlayout.setComponentAlignment(BtnLayout,Alignment.TOP_RIGHT);
									UserID = -1;
								}
							});
							btnUnsubscribe.addClickListener(new Button.ClickListener() {
								public void buttonClick(ClickEvent event) {

									CreateYesNoDialog("UNSUBSCRIBE","Are you sure you want to unsubscribe?\nThis action is irreversible!");		

								}
							});

							TxtLayoutWelcome.addComponent(welcomeMsg);
							TxtLayoutWelcome.setSizeFull();
							BtnLayoutWelcome.setSpacing(true);
							BtnLayoutWelcome.addComponent(btnSettings);
							BtnLayoutWelcome.addComponent(btnSignOut);
							BtnLayoutWelcome.addComponent(btnUnsubscribe);
							LayoutWelcome.setSpacing(true);
							LayoutWelcome.setSizeFull();
							LayoutWelcome.addComponent(TxtLayoutWelcome);
							LayoutWelcome.addComponent(BtnLayoutWelcome);
							//LayoutWelcome.setExpandRatio(TxtLayoutWelcome,1.0f);
							LayoutWelcome.setComponentAlignment(TxtLayoutWelcome, Alignment.BOTTOM_LEFT);
							LayoutWelcome.setComponentAlignment(BtnLayoutWelcome, Alignment.BOTTOM_RIGHT);
							Mainlayout.removeAllComponents();
							//Mainlayout.setSizeFull();
							Mainlayout.addComponent(LayoutWelcome);
							CreateSettingsWindow(0);
							//Mainlayout.setComponentAlignment(LayoutWelcome, Alignment.TOP_LEFT);

						}else{
							Notification.show("Error","Not all fields are valid!",Notification.Type.WARNING_MESSAGE);
						}
					}
				}
			}
		});
		btnSUCancel.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				SignUpWindow.close();
			}
		});



		btnSUSignUp.setStyleName("login");
		MainSULayout.setSpacing(true);
		MainSULayout.setMargin(true);
		BtnSULayout.setSpacing(true);
		BtnSULayout.setSizeFull();
		BtnSULayout.addComponent(btnSUSignUp);
		BtnSULayout.addComponent(btnSUCancel);
		BtnSULayout.setComponentAlignment(btnSUSignUp, Alignment.BOTTOM_LEFT);
		BtnSULayout.setComponentAlignment(btnSUCancel, Alignment.BOTTOM_RIGHT);
		MainSULayout.addComponent(txtSUFirsName);
		MainSULayout.addComponent(txtSULastName);
		MainSULayout.addComponent(txtSUEmail);
		MainSULayout.addComponent(txtSUPassword);
		MainSULayout.addComponent(txtSUConfirmPassword);
		MainSULayout.addComponent(BtnSULayout);
		SignUpWindow = new Window();
		SignUpWindow.setStyleName("winlogin");
		SignUpWindow.setModal(true);
		addWindow(SignUpWindow);
		txtSUFirsName.focus();
		SignUpWindow.setWidth("350px");
		SignUpWindow.setCaption("SIGN UP");
		//WindowRadnik.setHeight("450px");
		SignUpWindow.setHeightUndefined();
		SignUpWindow.setResizable(false);
		SignUpWindow.center();
		SignUpWindow.setContent(MainSULayout);

	}
	private void CreateLoginWindow(){

		MainWLLayout = new VerticalLayout();
		BtnWLLayout = new HorizontalLayout();
		LayoutWelcome = new HorizontalLayout();
		TxtLayoutWelcome = new HorizontalLayout();
		BtnLayoutWelcome = new HorizontalLayout();
		txtWLEmail = new MyTextField("E-mail:");
		txtWLPassword = new PasswordField ("Password:");
		txtWLPassword.setImmediate(true);
		btnWLLogin = new Button("Login");
		btnWLCancel = new Button("Cancel");
		txtWLEmail.setSizeFull();
		txtWLEmail.setInputPrompt("Enter e-mail...");
		txtWLPassword.setSizeFull();
		txtWLPassword.setInputPrompt("Enter password...");
		btnWLLogin.setStyleName("login");
		MainWLLayout.setSpacing(true);
		MainWLLayout.setMargin(true);
		BtnWLLayout.setSpacing(true);
		BtnWLLayout.setSizeFull();
		//txtEmail.setImmediate(true);
		//txtPassword.setImmediate(true);

		txtWLEmail.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				userEmail = txtWLEmail.getValue();
			}
		});
		txtWLPassword.addBlurListener(new BlurListener() {
			@Override
			public void blur(BlurEvent event) {
				userPass = txtWLPassword.getValue();
			}
		});
		OnEnterKeyHandler onEnterHandler = new OnEnterKeyHandler(){
            @Override
            public void onEnterKeyPressed() {
            	btnWLLogin.click();
            }
        };
        onEnterHandler.installOn(txtWLPassword);


		btnWLCancel.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				LoginWindow.close();
			}

		});
		btnWLLogin.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				try{
					Class.forName("com.mysql.jdbc.Driver").newInstance();
					String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
					String connectionUser = "root";
					String connectionPassword = "Code_Cons_Rulez1";
					conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
					stmt = (Statement) conn.createStatement();
					QRYUser = "SELECT iduser,password FROM hnbexchangewatcherschema.user WHERE e_mail = @email";
					QRYUser = QRYUser.replace("@email", "'"+userEmail+"'");
					//QRYUser = QRYUser.replace("@password", "'"+userPass+"'");
					rs = stmt.executeQuery(QRYUser);
					while (rs.next()) {
						UserID = rs.getInt(1);
						salted = rs.getString(2);
					}

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
					try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
					try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
				}
				try {
					if(UserID < 0 || !(validatePassword(userPass, salted)) ){
						Notification.show("Error","Invalid E-mail or Password",Notification.Type.WARNING_MESSAGE);
					}else{
						try{
							Class.forName("com.mysql.jdbc.Driver").newInstance();
							String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
							String connectionUser = "root";
							String connectionPassword = "Code_Cons_Rulez1";
							conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
							stmt = (Statement) conn.createStatement();
							QRYUserData = "SELECT first_name, last_name FROM hnbexchangewatcherschema.user WHERE iduser = @idUser";
							QRYUserData = QRYUserData.replace("@idUser", UserID+"");
							rs = stmt.executeQuery(QRYUserData);
							while (rs.next()) {
								userFirstName = rs.getString(1);
								userLastName = rs.getString(2);
							}
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
							try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
							try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
						}
						LoginWindow.close();
						welcomeMsg = new TextField();
						welcomeMsg.setValue("Welcome, "+userFirstName+" "+userLastName);
						welcomeMsg.setEnabled(false);
						welcomeMsg.setWidth("100%");
						welcomeMsg.setStyleName("lblwelcome");
						btnSettings = new Button("Settings");
						btnUnsubscribe = new Button("Unsubscribe");
						btnUnsubscribe.setStyleName("unsubscribe");
						btnSettings.setStyleName("settings");
						btnSignOut = new Button("Sign Out");
						btnSettings.addClickListener(new Button.ClickListener() {
							public void buttonClick(ClickEvent event) {
								CreateSettingsWindow(1);
							}
						});
						btnSignOut.addClickListener(new Button.ClickListener() {
							public void buttonClick(ClickEvent event) {
								Mainlayout.removeAllComponents();
								Mainlayout.addComponent(BtnLayout);
								Mainlayout.setComponentAlignment(BtnLayout,Alignment.TOP_RIGHT);
								UserID = -1;
							}
						});
						btnUnsubscribe.addClickListener(new Button.ClickListener() {
							public void buttonClick(ClickEvent event) {

								CreateYesNoDialog("UNSUBSCRIBE","Are you sure you want to unsubscribe?\nThis action is irreversible!");		

							}
						});


						TxtLayoutWelcome.addComponent(welcomeMsg);
						TxtLayoutWelcome.setSizeFull();
						BtnLayoutWelcome.setSpacing(true);
						BtnLayoutWelcome.addComponent(btnSettings);
						BtnLayoutWelcome.addComponent(btnSignOut);
						BtnLayoutWelcome.addComponent(btnUnsubscribe);
						LayoutWelcome.setSpacing(true);
						LayoutWelcome.setSizeFull();
						LayoutWelcome.addComponent(TxtLayoutWelcome);
						LayoutWelcome.addComponent(BtnLayoutWelcome);
						//LayoutWelcome.setExpandRatio(TxtLayoutWelcome,1.0f);
						LayoutWelcome.setComponentAlignment(TxtLayoutWelcome, Alignment.BOTTOM_LEFT);
						LayoutWelcome.setComponentAlignment(BtnLayoutWelcome, Alignment.BOTTOM_RIGHT);
						Mainlayout.removeAllComponents();
						//Mainlayout.setSizeFull();
						Mainlayout.addComponent(LayoutWelcome);
						//Mainlayout.setComponentAlignment(LayoutWelcome, Alignment.TOP_LEFT);
					}
				} catch (NoSuchAlgorithmException | InvalidKeySpecException | ReadOnlyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		BtnWLLayout.addComponent(btnWLLogin);
		BtnWLLayout.addComponent(btnWLCancel);
		BtnWLLayout.setComponentAlignment(btnWLLogin, Alignment.BOTTOM_LEFT);
		BtnWLLayout.setComponentAlignment(btnWLCancel, Alignment.BOTTOM_RIGHT);
		MainWLLayout.addComponent(txtWLEmail);
		MainWLLayout.addComponent(txtWLPassword);
		MainWLLayout.addComponent(BtnWLLayout);
		LoginWindow = new Window();
		LoginWindow.setStyleName("winlogin");
		LoginWindow.setModal(true);
		addWindow(LoginWindow);
		txtWLEmail.focus();
		LoginWindow.setWidth("350px");
		LoginWindow.setCaption("LOGIN");
		//WindowRadnik.setHeight("450px");
		LoginWindow.setHeightUndefined();
		LoginWindow.setResizable(false);
		LoginWindow.center();
		LoginWindow.setContent(MainWLLayout);
	}

	private void CreateYesNoDialog(String title,String message){
		MainYesNoDialogLayout = new VerticalLayout();
		YesNoDialogBtnLayout = new HorizontalLayout();
		btnYes = new Button("Yes");
		btnYes.setStyleName("unsubscribe");
		btnNo = new Button("No");
		btnNo.setStyleName("login");
		YesNoDialogMessage = new Label(message);
		YesNoDialogBtnLayout.setSpacing(true);
		YesNoDialogBtnLayout.setSizeFull();
		YesNoDialogBtnLayout.addComponent(btnYes);
		YesNoDialogBtnLayout.addComponent(btnNo);
		YesNoDialogBtnLayout.setComponentAlignment(btnYes, Alignment.BOTTOM_LEFT);
		YesNoDialogBtnLayout.setComponentAlignment(btnNo, Alignment.BOTTOM_RIGHT);
		MainYesNoDialogLayout.setSpacing(true);
		MainYesNoDialogLayout.setMargin(true);
		MainYesNoDialogLayout.addComponent(YesNoDialogMessage);
		MainYesNoDialogLayout.addComponent(YesNoDialogBtnLayout);
		YesNoDialog = new Window();
		YesNoDialog.setStyleName("winwarning");
		YesNoDialog.setModal(true);
		addWindow(YesNoDialog);
		YesNoDialog.setWidth("350px");
		YesNoDialog.setCaption(title);
		//WindowRadnik.setHeight("450px");
		YesNoDialog.setHeightUndefined();
		YesNoDialog.setResizable(false);
		YesNoDialog.center();
		YesNoDialog.setContent(MainYesNoDialogLayout);
		btnYes.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				@SuppressWarnings("unused")
				int i;
				try{
					Class.forName("com.mysql.jdbc.Driver").newInstance();
					String connectionUrl = "jdbc:mysql://localhost:3306/hnbexchangewatcherschema";
					String connectionUser = "root";
					String connectionPassword = "Code_Cons_Rulez1";
					conn = (Connection) DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
					stmt = (Statement) conn.createStatement();
					QRYDeleteUser = "DELETE FROM hnbexchangewatcherschema.user WHERE iduser = @idUser";
					QRYDeleteUser = QRYDeleteUser.replace("@idUser", UserID+"");
					i = stmt.executeUpdate(QRYDeleteUser);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
					try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
					try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
					Mainlayout.removeAllComponents();
					Mainlayout.addComponent(BtnLayout);
					Mainlayout.setComponentAlignment(BtnLayout,Alignment.TOP_RIGHT);
					UserID = -1;
					YesNoDialog.close();
				}	
			}
		});
		btnNo.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				YesNoDialog.close();
			}
		});
	}
	private void OpenWarninigDialog(String message){
		VerticalLayout MainDialogLayout = new VerticalLayout();
		HorizontalLayout BtnLayout = new HorizontalLayout();
		Button btnOk = new Button("OK");
		btnOk.setStyleName("login");
		Label Msg = new Label(message);
		
		BtnLayout.setSpacing(true);
		BtnLayout.setSizeFull();
		BtnLayout.addComponent(btnOk);
		BtnLayout.setComponentAlignment(btnOk, Alignment.BOTTOM_CENTER);
		MainDialogLayout.setSpacing(true);
		MainDialogLayout.setMargin(true);
		MainDialogLayout.addComponent(Msg);
		MainDialogLayout.addComponent(BtnLayout);
		
		WarningDialog = new Window();
		WarningDialog.setStyleName("winwarning");
		WarningDialog.setModal(true);
		addWindow(WarningDialog);
		WarningDialog.setWidth("350px");
		WarningDialog.setCaption("WARNING");
		//WindowRadnik.setHeight("450px");
		WarningDialog.setHeightUndefined();
		WarningDialog.setResizable(false);
		WarningDialog.center();
		WarningDialog.setContent(MainDialogLayout);
		
		btnOk.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				WarningDialog.close();
			}
		});
	}
	
	boolean CheckIfCurEnabled(){
		
		if(!chkAUD.getValue() && !chkCAD.getValue() && !chkCHF.getValue() && !chkCZK.getValue() && !chkDKK.getValue()
		&& !chkEUR.getValue() && !chkGBP.getValue() && !chkHUF.getValue() && !chkJPY.getValue() && !chkNOK.getValue()
		&& !chkPLN.getValue() && !chkSEK.getValue() && !chkUSD.getValue()){
			return false;	
		}else{
			return true;
		}
	}
	public String param(String Qry,int param){
		String qry;
		qry = Qry;
		qry = qry.replace("@idUser", param+"");
		//System.out.println(qry);
		return qry;
	}
	
	private static String generateStrongPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();
         
        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }
     
    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }
     
    private static String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }
    private static boolean validatePassword(String originalPassword, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        String[] parts = storedPassword.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);
         
        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();
         
        int diff = hash.length ^ testHash.length;
        for(int i = 0; i < hash.length && i < testHash.length; i++)
        {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }
    private static byte[] fromHex(String hex) throws NoSuchAlgorithmException
    {
        byte[] bytes = new byte[hex.length() / 2];
        for(int i = 0; i<bytes.length ;i++)
        {
            bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
	
	
}