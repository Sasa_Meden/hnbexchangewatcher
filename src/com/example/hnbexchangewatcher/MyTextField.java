package com.example.hnbexchangewatcher;

import com.vaadin.data.Property;
import com.vaadin.ui.TextField;

public class MyTextField extends TextField{

	/**
	 * My Text Field
	 */
	private static final long serialVersionUID = 7046063342440585620L;
	private boolean trim = true;
	
	public MyTextField(){
		super();
		setNullRepresentation("");
	}
	public MyTextField(Property<?> dataSource) {
		super(dataSource);
		setNullRepresentation("");
	}
	public MyTextField(String caption, Property<?> dataSource) {
		super(caption, dataSource);
		setNullRepresentation("");
	}
	public MyTextField(String caption, String value) {
		super(caption, value);
		setNullRepresentation("");
	}
	public MyTextField(String caption) {
		super(caption);
		setNullRepresentation("");
	}
	
	public MyTextField(boolean readOnly){
		super();
		setReadOnly(readOnly);
		setNullRepresentation("");
	}
	public MyTextField(Property<?> dataSource, boolean readOnly) {
		super(dataSource);
		setNullRepresentation("");
		setReadOnly(readOnly);
	}
	public MyTextField(String caption, Property<?> dataSource, boolean readOnly) {
		super(caption, dataSource);
		setNullRepresentation("");
		setReadOnly(readOnly);
	}
	public MyTextField(String caption, String value, boolean readOnly) {
		super(caption, value);
		setNullRepresentation("");
		setReadOnly(readOnly);
	}
	public MyTextField(String caption, boolean readOnly) {
		super(caption);
		setNullRepresentation("");
		setReadOnly(readOnly);
	}

	@Override
	public String getValue(){
		if(!trim || super.getValue() == null)
			return super.getValue();
		else
			return super.getValue().trim();
	}
	
	@Override
	public void setValue(String newValue){
		boolean wasReadOnly = isReadOnly();
		if(wasReadOnly)
			setReadOnly(false);
		if(!trim || newValue == null)
			super.setValue(newValue);
		else
			super.setValue(newValue.trim());
		if(wasReadOnly)
			setReadOnly(true);
	}
	
	public void setValue(Integer value){
		if(value == null)
			setValue((String)null);
		else
			setValue(String.valueOf(value));
	}
	public void setValue(Double value){
		if(value == null)
			setValue((String)null);
		else
			setValue(String.valueOf(value));
	}
	public void setValue(Float value){
		if(value == null)
			setValue((String)null);
		else
			setValue(String.valueOf(value));
	}

	public boolean isTrim() {
		return trim;
	}

	public void setTrim(boolean trim) {
		this.trim = trim;
	}
	
}
