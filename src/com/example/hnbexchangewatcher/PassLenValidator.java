package com.example.hnbexchangewatcher;

import com.vaadin.data.Validator;

public class PassLenValidator implements Validator{
	/**
	 * Pass Lenght Validator
	 */
	
	private static final long serialVersionUID = -596453333979004900L;
	public boolean isValid(Object value) {
        try {
            validate(value);
        } catch (Validator.InvalidValueException ive) {
            return false;
        }
        return true;
    }
	@Override
	public void validate(Object value) throws InvalidValueException {
		// Here value is a String
        if (value instanceof String) {
            String strValue = (String)value;
            // Check if value is empty
            if (strValue.length()<6)
                throw new Validator.InvalidValueException("Password must be at least 6 characters long");
        }
    
	}
}
