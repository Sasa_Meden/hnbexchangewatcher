package com.example.hnbexchangewatcher;

import com.vaadin.data.Validator;

public class PasswordValidator implements Validator {

	

	/**
	 * Validates Password Confirmation
	 */
	private static final long serialVersionUID = 5175535301987656276L;
	private String confirm = "";
	public PasswordValidator(String pass){
		this.confirm = pass;
	}
	public boolean isValid(Object value) {
        try {
            validate(value);
        } catch (Validator.InvalidValueException ive) {
            return false;
        }
        return true;
    }
	@Override
	public void validate(Object value) throws InvalidValueException {
		// Here value is a String containing password
        if (value instanceof String) {
            String pass = (String)value;
            // Match the password against conformation
            if (!pass.equals(confirm))
                throw new Validator.InvalidValueException("Passwords do not match!");
        }
    }

}
